#ifndef __eu_net_c__
#define __eu_net_c__

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define _NET_SYNCHRONOUS     0
#define _NET_ASYNCHRONOUS    1

#define _NET_GENERROR       -1
#define _NET_UNKNOWN_HOST   -2

int netu_listenSocket(char* lAddr, int port, char async, struct sockaddr_in* sInfo);
char* netu_resolvehost(char* rAddr);
int netu_clientSocket(char* rAddr, int port);

#endif
