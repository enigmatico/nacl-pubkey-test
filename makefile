all:
	gcc -g -c net.c -o net.o
	gcc -g -c main.c -o main.o -D__STATIC_NACL__
	gcc -g main.o -o main ./slibs/bin/libsodium.a
	gcc -g -c bob.c -o bob.o -D__STATIC_NACL__
	gcc -g -c alice.c -o alice.o -D__STATIC_NACL__
	gcc -g net.o bob.o -o bob ./slibs/bin/libsodium.a
	gcc -g net.o alice.o -o alice ./slibs/bin/libsodium.a

unsafe:
	gcc -g -c net.c -o net.o
	gcc -g -c main.c -o main.o
	gcc -g main.o -o main -lsodium
	gcc -g -c bob.c -o bob.o
	gcc -g -c alice.c -o alice.o
	gcc -g net.o bob.o -o bob -lsodium
	gcc -g net.o alice.o -o alice -lsodium
	
clean:
	rm -rf *.o
	rm main
	rm bob
	rm alice
