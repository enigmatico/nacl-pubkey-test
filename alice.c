#if defined(__linux__)
# include <fcntl.h>
# include <unistd.h>
# include <sys/ioctl.h>
# include <linux/random.h>
#include <sys/syscall.h>
#endif

#include <stdio.h> 
#include <stdint.h>
#include <string.h>

#include "net.h"
#ifdef __STATIC_NACL__
    #include "slibs/include/sodium.h"
#else
    #include <sodium.h>
#endif

void clearBuffer(char* buf)
{
    memset(buf, 0L, 8192);
    return;
}

void genNonce(unsigned char* nBuf, unsigned long long int nSiz, int passes)
{
    int x = 0;
    for(x = 0; x < passes; x++)
    {
        randombytes_buf(nBuf, nSiz);
    }
    return;
}

int main(int args, char** argv)
{
    /* Determine if your system has enough entropy.
     * This has  been taken from NaCl's documentation. */
    
    #if defined(__linux__) && defined(RNDGETENTCNT)
    int fd;
    int c;

    if ((fd = open("/dev/random", O_RDONLY)) != -1) {
        if (ioctl(fd, RNDGETENTCNT, &c) == 0 && c < 160) {
            fputs("This system doesn't provide enough entropy to quickly generate high-quality random numbers.\n"
                "Installing the rng-utils/rng-tools, jitterentropy or haveged packages may help.\n"
                "On virtualized Linux environments, also consider using virtio-rng.\n"
                "The service will not start until enough entropy has been collected.\n", stderr);
        }else{
            printf("This system has enough entropy.\n");
        }
        (void) close(fd);
    }
    #endif

    /* Initialize libsodium */
    if(sodium_init() < 0)
    {
        printf("libsodium could not be initialized\n");
        return 1;
    }
    
    printf("Libsodium initialized\n");
    
    // Network
    struct sockaddr_in sClient;
    unsigned char netbuf[8192] = { 0x0 };
    
    // Keypairs
    unsigned char alice_pk[crypto_kx_PUBLICKEYBYTES], alice_sk[crypto_kx_SECRETKEYBYTES];
    unsigned char bob_pk[crypto_kx_PUBLICKEYBYTES] = { 0x0 };
    
    // Nonce
    unsigned char nonce[crypto_box_NONCEBYTES];
    
    // Authentication and messages
    const unsigned char* message = "Hello Bob! I am Alice.";
    unsigned char decmsg[4089] = { 0x0 };
    int32_t clen = 0, dlen = 0, csock = 0L, csz = 0, rs = 0, trs = 0;
    
    /* Generate the keypairs */
    if(crypto_box_keypair(alice_pk, alice_sk) < 0)
    {
        printf("Uhm... there was a problem generating the keypair\n");
        return 0;
    }
    
    // Connect with Bob (Server)
    csock = netu_clientSocket("127.0.0.1", 7080);
    
    if(csock)
    {
        // Generate a Nonce
        genNonce(nonce, crypto_box_NONCEBYTES, 20);
        
        int32_t curs = 0, slen = 0;
        
        // Send Alice's (Client) public key

        rs = send(csock, alice_pk, crypto_kx_PUBLICKEYBYTES, 0);
        printf("Sent PK (Alice's) %i bytes out of %i\n", rs, crypto_kx_PUBLICKEYBYTES);
        
        // Get Bob's (Server) public key
        do{
            rs = recv(csock, ((void*)bob_pk)+trs, crypto_kx_PUBLICKEYBYTES, 0);
            trs += rs;
        }while(rs > 0 && trs < crypto_kx_PUBLICKEYBYTES);
        printf("Received Bob's PK (%i bytes) out of %i bytes\n", trs, crypto_kx_PUBLICKEYBYTES);
        trs = 0;
        
        // Generate a new cyphered message
        if( (clen = crypto_box_easy(netbuf, message,strlen(message), nonce, bob_pk, alice_sk)) < 0)
        {
            printf("Message not encrypted (%i)\n", clen);
            shutdown(csock, 2);
            close(csock);
            return 1;
        }else{
            printf("Message successfuly encrypted\n%s\n",netbuf);
        }
        
        // Send the nonce
        rs = send(csock, nonce, crypto_box_NONCEBYTES, 0);
        printf("Sent nonce %i bytes out of %i\n", rs, crypto_box_NONCEBYTES);
        
        // Send the length of the cyphered block
        int32_t cbsl = crypto_box_MACBYTES + strlen(message);
        rs = send(csock, &cbsl, sizeof(int32_t), 0);
        printf("Sent length of cypher %i bytes out of %i\n", rs, sizeof(int32_t));
        
        // Send the cyphered block
        rs = send(csock, netbuf, cbsl, 0);
        printf("Sent cypher %i bytes out of %i\n", rs, cbsl);
        
        // Clear the buffer
        clearBuffer(netbuf);
        
        // Get the nonce
        do{
            rs = recv(csock, ((void*)nonce)+trs, crypto_box_NONCEBYTES, 0);
            trs += rs;
        }while(rs > 0 && trs < crypto_box_NONCEBYTES);
        printf("Received nonce (%i bytes) out of %i bytes\n", trs, crypto_box_NONCEBYTES);
        trs = 0;
        
        // Get the length of the cyphered block
        do{
            rs = recv(csock, ((void*)&curs)+trs, sizeof(int32_t), 0);
            trs += rs;
        }while(rs > 0 && rs < sizeof(int32_t));
        printf("Received length of cypher %i (%i bytes) out of %i bytes\n", curs, trs, sizeof(int32_t));
        trs = 0;
        
        // Get the cyphered block
        do{
            rs = recv(csock, ((void*)netbuf)+trs, curs, 0);
            trs += rs;
        }while(rs > 0 && trs < curs);
        printf("Received cypher (%i bytes) out of %i bytes\n", trs, curs);
        trs = 0;
        
        // Decypher the content
        if ( (dlen = crypto_box_open_easy(decmsg, netbuf, curs, nonce, bob_pk, alice_sk)) < 0)
        {
            printf("Message not decrypted ;_;\n");
            //return 1;
        }else{
            printf("Bob's Message: %s\n", decmsg);
        }
        
        
        // End the communitcation with the server
        shutdown(csock, 2);
        close(csock);
    }
    printf("Job's done!\n");
    return 0;
} 
