#if defined(__linux__)
# include <fcntl.h>
# include <unistd.h>
# include <sys/ioctl.h>
# include <linux/random.h>
#include <sys/syscall.h>
#endif

#include <stdio.h> 
#include <string.h>

#ifdef __STATIC_NACL__
    #include "slibs/include/sodium.h"
#else
    #include <sodium.h>
#endif

void genNonce(unsigned char* nBuf, unsigned long long int nSiz, int passes)
{
    int x = 0;
    for(x = 0; x < passes; x++)
    {
        randombytes_buf(nBuf, nSiz);
    }
    return;
}

int main(int args, char** argv)
{
    /* Determine if your system has enough entropy.
     * This has  been taken from NaCl's documentation. */
    
    #if defined(__linux__) && defined(RNDGETENTCNT)
    int fd;
    int c;

    if ((fd = open("/dev/random", O_RDONLY)) != -1) {
        if (ioctl(fd, RNDGETENTCNT, &c) == 0 && c < 160) {
            fputs("This system doesn't provide enough entropy to quickly generate high-quality random numbers.\n"
                "Installing the rng-utils/rng-tools, jitterentropy or haveged packages may help.\n"
                "On virtualized Linux environments, also consider using virtio-rng.\n"
                "The service will not start until enough entropy has been collected.\n", stderr);
        }else{
            printf("This system has enough entropy.\n");
        }
        (void) close(fd);
    }
    #endif

    /* Initialize libsodium */
    if(sodium_init() < 0)
    {
        printf("libsodium could not be initialized\n");
        return 1;
    }
    
    printf("Libsodium initialized\n");
    
    //Keypairs
    unsigned char client_pk[crypto_kx_PUBLICKEYBYTES], client_sk[crypto_kx_SECRETKEYBYTES];
    unsigned char server_pk[crypto_kx_PUBLICKEYBYTES], server_sk[crypto_kx_SECRETKEYBYTES];
    
    //Nonce
    unsigned char nonce[crypto_box_NONCEBYTES];
    //int r = syscall(SYS_getrandom, nonce, crypto_box_NONCEBYTES, 1);
    genNonce(nonce, crypto_box_NONCEBYTES, 20);
    
    //Authentication and messages
    const unsigned char* message = "Bob and Alice.";
    unsigned char authenc[crypto_box_MACBYTES + strlen(message)];
    unsigned char decmsg[4089] = { 0x0 };
    int clen = 0, dlen = 0;
    
    /* Generate the keypairs */
    if(crypto_box_keypair(client_pk, client_sk) < 0)
    {
        printf("Uhm... there was a problem generating the client keypair\n");
        return 0;
    }
    
    if(crypto_box_keypair(server_pk, server_sk) < 0)
    {
        printf("Uhm... there was a problem generating the server keypair\n");
        return 0;
    }
    
    
    /* Both Client - Server are assumed to have their public keys shared
     * between each other */
    
    /* Client -> Server message encryption */
    
    if( (clen = crypto_box_easy(authenc, message,strlen(message), nonce, server_pk, client_sk)) < 0)
    {
        printf("Message not encrypted\n");
        return 1;
    }else{
        printf("Message successfuly encrypted\n%s\n",authenc);
    }
    
    /* Server-side message decryption */
    
    if ( (dlen = crypto_box_open_easy(decmsg, authenc, crypto_box_MACBYTES + strlen(message), nonce, client_pk, server_sk)) < 0)
    {
        printf("Message not decrypted ;_;\n");
        return 1;
    }else{
        printf("Message: %s\n", decmsg);
    }
    
    /* Nonce must be regenerated for the next message */
    // genNonce(nonce, crypto_box_NONCEBYTES, 20);
    
    return 0;
}
