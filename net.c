#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <netdb.h>
#include <fcntl.h>
#include <pthread.h>

#include "net.h"

int netu_listenSocket(char* lAddr, int port, char async, struct sockaddr_in* sInfo)
{
	int socket_desc = socket(AF_INET , SOCK_STREAM , 0);
	struct sockaddr_in server;

	if(socket_desc < 0)
	{
		return _NET_GENERROR;
	}
	server.sin_family = AF_INET;
	if(lAddr)
	{
		server.sin_addr.s_addr = inet_addr(lAddr);
	}else{
		server.sin_addr.s_addr = INADDR_ANY;
	}
	
	server.sin_port = htons(port);
	if(bind(socket_desc, (struct sockaddr *)&server, sizeof(server))<0)
	{
		return -errno;
	}else{
		if(async > 0)
			fcntl(socket_desc, O_NONBLOCK);
		
		if(sInfo)
		{
			memmove(sInfo, &server, sizeof(struct sockaddr_in));
		}
		
		return socket_desc;
	}
}

char* netu_resolvehost(char* rAddr)
{
	char* pAddr = (char*)calloc(1,40);
	struct hostent *he;
	struct in_addr** addr_list;
	if( !(he = gethostbyname(rAddr)) )
	{
		return NULL;
	}
	
	addr_list = (struct in_addr **) he->h_addr_list;
	strcpy(pAddr, inet_ntoa(*addr_list[0]));
	return pAddr;
}

int netu_clientSocket(char* rAddr, int port)
{
	char* remoteaddr;
	int socket_desc = socket(AF_INET, SOCK_STREAM, 0);
	struct sockaddr_in server;
	struct addrinfo* remote;
	if(socket_desc < 0)
	{
		return _NET_GENERROR;
	}
	
	remoteaddr = netu_resolvehost(rAddr);	
	if(!remoteaddr)
	{
		return _NET_UNKNOWN_HOST;
	}
	
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = inet_addr(remoteaddr);
	server.sin_port = htons(port);
	
	free(remoteaddr);
	
	if(connect(socket_desc, (struct sockaddr *)&server, sizeof(server)) < 0)
	{
		return -errno;
	}else{
		return socket_desc;
	}
}
