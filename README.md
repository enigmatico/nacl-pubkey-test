This is a test I made on how to perform a server-client 
communication over TCP using public key encryption with libsodium.

There are two ways to make this work.
One, get the source of libsodium, and place the static library files 
in slibs/bin and the headers in include, then compile with make.

The other way is installing libsodium on your system, and compiling 
using make unsafe

Using shared libraries is considered a unsafe practise. However, 
this is just a test and you can do whatever you want.

Then run ./bob (server), and ./alice (client) in your computer.
You should see both executables sharing messages.
